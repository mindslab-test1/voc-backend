## maum-VOC backend

### 개발스택
* spring-webflux (netty)
* spring-data-r2dbc (mariadb)
* spring-data-elasticsearch (reactive)
* spring-data-redis (reactive)
* spring-security (reactive)
* JWT
* RESTful