package ai.maum.voc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@SpringBootApplication
@EnableR2dbcRepositories
public class VocBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(VocBackendApplication.class, args);
    }

}
