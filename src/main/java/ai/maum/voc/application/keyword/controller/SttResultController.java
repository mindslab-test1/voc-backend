package ai.maum.voc.application.keyword.controller;

import ai.maum.voc.application.keyword.model.response.SttResultResponse;
import ai.maum.voc.application.keyword.service.SttResultService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * ai.maum.voc.application.keyword.controller
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-16
 */
@RestController
@RequiredArgsConstructor
public class SttResultController {

    private final SttResultService sttResultService;

    /**
     * test rest api
     * @return
     */
    @GetMapping("/elastic-search/stt")
    public Mono<SttResultResponse> getSttResults(){
        return sttResultService.getSttResultAggs().log();
    }

}
