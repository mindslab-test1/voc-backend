package ai.maum.voc.application.keyword.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.*;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * ai.maum.voc.application.keyword.entity
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-16
 */
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SttResult implements Serializable {

    private static final long serialVersionUID = 6189827187839457897L;

    @Id
    private String _id;

    private Integer sttResultDetailId;

    private Integer sttResultId;

    private Integer callId;

    private String speakerCode;

    private Integer sentenceId;

    private String sentence;

    private Double startTime;

    private Double endTime;

    private Float speed;

    private String silenceYn;

    private String ignored;

    private Integer creatorId;

    private Integer updaterId;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime updatedDtm;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createdDtm;

    private List<String> sentenceKeyword;

}
