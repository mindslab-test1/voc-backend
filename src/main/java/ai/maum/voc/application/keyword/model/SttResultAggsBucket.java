package ai.maum.voc.application.keyword.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * ai.maum.voc.application.keyword.model
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-16
 */
@Data
@AllArgsConstructor
public class SttResultAggsBucket {

    private String key;
    private Long count;

}
