package ai.maum.voc.application.keyword.model.response;

import ai.maum.voc.application.keyword.model.SttResultAggsBucket;
import lombok.Builder;
import lombok.Data;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

/**
 * ai.maum.voc.application.keyword.model.response
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-16
 */
@Data
@Builder
public class SttResultResponse {

    Long totalCount;
    List<SttResultAggsBucket> sttResultAggsBuckets;

    /**
     * SttResultResponse 가공
     * @param aggregations es agg result
     * @return
     */
    public static Mono<SttResultResponse> of(List<Aggregation> aggregations){
        return Mono.just(
                SttResultResponse.builder()
                        .totalCount(
                                aggregations.stream().flatMap(aggregation -> ((ParsedStringTerms) aggregation).getBuckets().stream()).mapToLong(MultiBucketsAggregation.Bucket::getDocCount).sum()
                        )
                        .sttResultAggsBuckets(
                                aggregations.stream()
                                        .flatMap(aggregation -> ((ParsedStringTerms) aggregation).getBuckets().stream())
                                        .map(bucket -> new SttResultAggsBucket(bucket.getKeyAsString(), bucket.getDocCount()))
                                        .collect(Collectors.toList())
                        )
                        .build()
        ).log();
    }

}
