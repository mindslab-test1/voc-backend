package ai.maum.voc.application.keyword.repository;

import ai.maum.voc.application.keyword.entity.SttResult;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * ai.maum.voc.application.keyword.repository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-17
 */
@Repository
public interface SttResultRepository extends CrudRepository<SttResult, String> {

}
