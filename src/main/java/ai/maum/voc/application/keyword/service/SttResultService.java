package ai.maum.voc.application.keyword.service;

import ai.maum.voc.application.keyword.model.response.SttResultResponse;
import lombok.RequiredArgsConstructor;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * ai.maum.voc.application.keyword.service
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-16
 */
@Service
@RequiredArgsConstructor
public class SttResultService {

    private final ReactiveElasticsearchClient reactiveElasticsearchClient;

    /**
     * test search
     * @return aggs
     */
    public Mono<SttResultResponse> getSttResultAggs(){
        return reactiveElasticsearchClient.searchForResponse(new SearchRequest("stt-test")
                .source(
                        SearchSourceBuilder.searchSource()
                                .trackTotalHits(true)
                                .query(
                                        QueryBuilders.boolQuery()
                                                .filter(
                                                        QueryBuilders.rangeQuery("createdDtm")
                                                                .gte("2021-01-04T10:28:58")
                                                                .lte("2021-12-04T10:30:58")
                                                ).filter(
                                                        QueryBuilders.termQuery("sentenceKeyword", "습니다")
                                                )
                                ).size(0)
                                .aggregation(
                                        AggregationBuilders.terms("sentenceKeyword")
                                                .script(
                                                        new Script("return doc['sentenceKeyword'].stream()\n" +
                                                                "                    .distinct()\n" +
                                                                "                    .filter(v -> !v.equals('습니다') && v.length() >= 3)\n" +
                                                                "                    .collect(Collectors.toList())")
                                                )
                                                .size(10)
                                )
                )
        ).flatMap(v -> SttResultResponse.of(v.getAggregations().asList())).log();
    }

}
