package ai.maum.voc.application.login.controller;

import ai.maum.voc.application.login.entity.UserInfo;
import ai.maum.voc.application.login.model.request.LoginRequest;
import ai.maum.voc.application.login.model.response.LoginResponse;
import ai.maum.voc.application.login.service.LoginService;
import ai.maum.voc.config.spring.webflux.annotation.LoginUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

/**
 * ai.maum.voc.application.login.controller
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-17
 */
@Slf4j
@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class LoginController {

    private final LoginService loginService;

    /**
     * 로그인
     * @param loginRequest 로그인 요청
     * @return 로그인 응답
     */
    @PostMapping("/sign-in")
    public Mono<LoginResponse> login(@RequestBody LoginRequest loginRequest){
        return loginService.login(loginRequest);
    }

    /**
     * 회원정보 조회
     * @param userInfo 회원정보 entity
     * @return 회원정보
     */
    @GetMapping("/me")
    public Mono<UserInfo> me(@LoginUserInfo UserInfo userInfo){
        return Mono.just(userInfo);
    }

}
