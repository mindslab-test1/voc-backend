package ai.maum.voc.application.login.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

/**
 * ai.maum.voc.application.login.entity
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-17
 */
@Data
@Builder
@RedisHash("userInfo")
public class UserInfo {

    @Id
    String userId;
    private String empNo;
    private String userNm;
    private String autCd;

}
