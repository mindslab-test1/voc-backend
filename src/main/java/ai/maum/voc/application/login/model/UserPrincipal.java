package ai.maum.voc.application.login.model;

import ai.maum.voc.application.user.entity.QaUsrMgtTb;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * ai.maum.voc.application.login.model
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-18
 */
public class UserPrincipal implements UserDetails {

    private QaUsrMgtTb qaUsrMgtTb;

    public UserPrincipal(QaUsrMgtTb qaUsrMgtTb){
        this.qaUsrMgtTb = qaUsrMgtTb;
    }

    public static UserPrincipal create(QaUsrMgtTb qaUsrMgtTb){
        return new UserPrincipal(qaUsrMgtTb);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(qaUsrMgtTb.getAutCd()));
    }

    @Override
    public String getPassword() {
        return qaUsrMgtTb.getUserPw();
    }

    @Override
    public String getUsername() {
        return qaUsrMgtTb.getUserId();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
