package ai.maum.voc.application.login.model.request;

import lombok.Data;

/**
 * ai.maum.voc.application.login.model.request
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-17
 */
@Data
public class LoginRequest {

    private String userId;
    private String userPw;

}
