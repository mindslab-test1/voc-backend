package ai.maum.voc.application.login.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * ai.maum.voc.application.login.model.response
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-17
 */
@Data
@AllArgsConstructor
public class LoginResponse {

    private String accessToken;

}
