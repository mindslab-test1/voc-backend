package ai.maum.voc.application.login.repository;

import ai.maum.voc.application.login.entity.UserInfo;
import org.springframework.data.repository.CrudRepository;

/**
 * ai.maum.voc.application.login.repository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-17
 */
public interface LoginRedisRepository extends CrudRepository<UserInfo, String> {



}
