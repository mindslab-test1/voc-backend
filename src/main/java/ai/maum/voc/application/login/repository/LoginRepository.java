package ai.maum.voc.application.login.repository;

import ai.maum.voc.application.user.entity.QaUsrMgtTb;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

/**
 * ai.maum.voc.application.login.repository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-17
 */
@Repository
public interface LoginRepository extends ReactiveCrudRepository<QaUsrMgtTb, String> {

    Mono<QaUsrMgtTb> findByUserIdEquals(String userId);

}
