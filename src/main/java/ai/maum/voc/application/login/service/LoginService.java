package ai.maum.voc.application.login.service;

import ai.maum.voc.application.login.entity.UserInfo;
import ai.maum.voc.application.login.model.request.LoginRequest;
import ai.maum.voc.application.login.model.response.LoginResponse;
import ai.maum.voc.application.login.repository.LoginRedisRepository;
import ai.maum.voc.application.login.repository.LoginRepository;
import ai.maum.voc.config.spring.security.jwt.JwtSignHandler;
import ai.maum.voc.exception.BadCredentialsException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Optional;

/**
 * ai.maum.voc.application.login.service
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-17
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class LoginService {

    private final LoginRepository loginRepository;
    private final LoginRedisRepository loginRedisRepository;
    private final JwtSignHandler jwtSignHandler;

    /**
     * login
     * @param loginRequest 로그인 요청
     * @return 로그인 결과 반환
     */
    public Mono<LoginResponse> login(LoginRequest loginRequest){
        return loginRepository.findByUserIdEquals(loginRequest.getUserId())
                .switchIfEmpty(Mono.error(new BadCredentialsException(HttpStatus.NOT_FOUND, "아이디 또는 비밀번호를 확인해주세요.")))
                .map(qaUsrMgtTb -> {
                    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
                    if(!encoder.matches(loginRequest.getUserPw(), qaUsrMgtTb.getUserPw())){
                        throw new BadCredentialsException(HttpStatus.FORBIDDEN, "아이디 또는 비밀번호를 확인해주세요.");
                    }
                    return qaUsrMgtTb;
                }).flatMap(qaUsrMgtTb -> Mono.fromCallable(() -> loginRedisRepository.save(UserInfo.builder().userId(qaUsrMgtTb.getUserId()).userNm(qaUsrMgtTb.getUserNm()).empNo(qaUsrMgtTb.getEmpNo()).autCd(qaUsrMgtTb.getAutCd()).build())).subscribeOn(Schedulers.boundedElastic()))
                .flatMap(userInfo -> Mono.just(new LoginResponse(jwtSignHandler.createAccessToken(userInfo.getUserId()))));
    }

    /**
     * 회원 정보 조회
     * @param userId - 회원 아이디
     * @return 회원 정보
     */
    public Mono<UserInfo> getUserInfo(String userId){
        return Mono.fromCallable(() -> loginRedisRepository.findById(userId))
                .subscribeOn(Schedulers.boundedElastic())
                .flatMap(optional -> optional.map(Mono::just).orElseGet(Mono::empty))
                .switchIfEmpty(
                        loginRepository.findByUserIdEquals(userId)
                                .switchIfEmpty(Mono.error(new BadCredentialsException(HttpStatus.NOT_FOUND, "아이디가 존재하지 않습니다.")))
                                .flatMap(qaUsrMgtTb -> Mono.fromCallable(() -> loginRedisRepository.save(UserInfo.builder().userId(qaUsrMgtTb.getUserId()).userNm(qaUsrMgtTb.getUserNm()).empNo(qaUsrMgtTb.getEmpNo()).autCd(qaUsrMgtTb.getAutCd()).build())))
                );
    }

}
