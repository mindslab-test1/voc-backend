package ai.maum.voc.application.stt.controller;

import ai.maum.voc.application.stt.model.response.SttResponse;
import ai.maum.voc.application.stt.service.SttService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * ai.maum.voc.application.stt.controller
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-12
 */
@RestController
@RequiredArgsConstructor
public class SttController {

    private final SttService sttService;

    @GetMapping("/stt")
    public Mono<PageImpl<SttResponse>> getSttResult(){
        return sttService.getSttResult(PageRequest.of(1, 100)).log();
    }

}