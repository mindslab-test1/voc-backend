package ai.maum.voc.application.stt.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;

/**
 * ai.maum.voc.application.stt.entity
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-12
 */
@Data
@RequiredArgsConstructor
@Table("QA_STT_RST_TB")
public class QaSttRstTb {

    @Id
    @Column("REC_KEY")
    private String recKey;

    @Column("REC_ID")
    private String recId;

    @Column("REC_FILE_NM")
    private String recFileNm;

    @Column("STMT_NO")
    private String stmtNo;

    @Column("SPK_DIV_CD")
    private String spkDivCd;

    @Column("STMT_ST_TM")
    private Duration stmtStTm;

    @Column("STMT_ED_TM")
    private Duration stmtEdTm;

    @Column("STMT")
    private String stmt;

    @Column("UPDATE_STMT")
    private String updateStmt;

    @Column("UPDATE_STMT_YN")
    private String updateStmtYn;

    @Column("SILENCE")
    private String silence;

    @Column("UPDATOR_ID")
    private String updatorId;

    @Column("UPDATED_TM")
    private LocalDateTime updatedTm;

    @Column("CREATOR_ID")
    private String creatorId;

    @Column("CREATED_TM")
    private LocalDateTime createdTm;

    @Column("SPK_SPD")
    private BigDecimal spkSpd;

}
