package ai.maum.voc.application.stt.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * ai.maum.voc.application.stt.entity
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-12
 */
@Data
@RequiredArgsConstructor
public class QaSttRstTbId {

    private String recKey;
    private String recId;
    private String recFileNm;
    private String stmtNo;

}
