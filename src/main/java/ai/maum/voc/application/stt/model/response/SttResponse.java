package ai.maum.voc.application.stt.model.response;

import ai.maum.voc.application.stt.entity.QaSttRstTb;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ai.maum.voc.application.stt.model.response
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-12
 */
@Data
@Builder
public class SttResponse {

    private String stmt;
    private String silence;
    private Duration stmtStTm;
    private Duration stmtEdTm;
    private String spkDivCd;
    private String creatorId;
    private LocalDateTime createdTm;
    private String recFileNm;
    private BigDecimal spkSpd;

    public static List<SttResponse> of(List<QaSttRstTb> list){
        return list.stream().map(v -> {
            return SttResponse.builder()
                    .stmt(v.getStmt())
                    .silence(v.getSilence())
                    .stmtStTm(v.getStmtStTm())
                    .stmtEdTm(v.getStmtEdTm())
                    .spkDivCd(v.getSpkDivCd())
                    .creatorId(v.getCreatorId())
                    .recFileNm(v.getRecFileNm())
                    .spkSpd(v.getSpkSpd())
                    .createdTm(v.getCreatedTm())
                    .build();
        }).collect(Collectors.toList());
    }

}
