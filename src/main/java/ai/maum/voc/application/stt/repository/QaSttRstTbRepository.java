package ai.maum.voc.application.stt.repository;

import ai.maum.voc.application.stt.entity.QaSttRstTb;
import ai.maum.voc.application.stt.entity.QaSttRstTbId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

/**
 * ai.maum.voc.application.stt.repository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-12
 */
public interface QaSttRstTbRepository extends ReactiveCrudRepository<QaSttRstTb, QaSttRstTbId> {

    Flux<QaSttRstTb> findAllBy(Pageable pageable);

}
