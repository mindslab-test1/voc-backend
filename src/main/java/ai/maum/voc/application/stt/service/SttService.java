package ai.maum.voc.application.stt.service;

import ai.maum.voc.application.stt.model.response.SttResponse;
import ai.maum.voc.application.stt.repository.QaSttRstTbRepository;
import ai.maum.voc.exception.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * ai.maum.voc.application.stt.service
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-12
 */
@Service
@RequiredArgsConstructor
public class SttService {

    private final QaSttRstTbRepository qaSttRstTbRepository;

    public Mono<PageImpl<SttResponse>> getSttResult(PageRequest request){
        return qaSttRstTbRepository.findAllBy(request)
                .log()
                .switchIfEmpty(Mono.error(new EntityNotFoundException(HttpStatus.NOT_FOUND, "STT 조회결과 없음")))
                .collectList()
                .zipWith(qaSttRstTbRepository.count().log())
                .map(v -> new PageImpl<>(SttResponse.of(v.getT1()), request, v.getT2()))
                .onErrorResume(Mono::error);
    }

}
