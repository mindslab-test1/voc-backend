package ai.maum.voc.application.user.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * ai.maum.voc.application.user.entity
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-17
 */
@Data
@RequiredArgsConstructor
@Table("QA_USR_MGT_TB")
public class QaUsrMgtTb {

    @Id
    private String userId;
    private String empNo;
    private String userNm;
    private String userPw;
    private int enabled;
    private String authority;
    private String autCd;
    private String updatorId;
    private LocalDateTime updatedTm;
    private String creatorId;
    private LocalDateTime createdTm;

}
