package ai.maum.voc.config.spring.security.jwt;

import ai.maum.voc.exception.JsonTokenParseException;
import ai.maum.voc.exception.UnauthorizedException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import reactor.core.publisher.Mono;

/**
 * ai.maum.voc.config.spring.security.jwt
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-17
 */
@Slf4j
public class CurrentUserAuthenticationBearer {

    public static Mono<Authentication> create(DecodedJWT decodedJWT) {
        String userId;
        try {
            userId = decodedJWT.getClaims().get("userId").asString();
        } catch (Exception e) {
            return Mono.error(new JsonTokenParseException(HttpStatus.INTERNAL_SERVER_ERROR, "json token parse error. invalid claims decoded JWT : " + decodedJWT.getClaims()));
        }
        return Mono.justOrEmpty(new CurrentUserAuthenticationToken(userId, decodedJWT));
    }


}
