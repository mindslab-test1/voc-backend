package ai.maum.voc.config.spring.security.jwt;

import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.security.authentication.AbstractAuthenticationToken;

/**
 * ai.maum.voc.config.spring.security.jwt
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-17
 */
public class CurrentUserAuthenticationToken extends AbstractAuthenticationToken {

    private final String userId;
    private final DecodedJWT decodedJwtToken;

    @Override
    public Object getCredentials() {
        return this.decodedJwtToken;
    }

    @Override
    public Object getPrincipal() {
        return this.userId;
    }

    public CurrentUserAuthenticationToken(String userId, DecodedJWT decodedJwtToken) {
        super(null);
        this.userId = userId;
        this.decodedJwtToken = decodedJwtToken;
        super.setAuthenticated(true);
    }

}