package ai.maum.voc.config.spring.security.jwt;

import lombok.Getter;

/**
 * ai.maum.voc.config.spring.security.jwt
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-18
 */
@Getter
public enum JwtSecrets {

    MINDS_SECRET("msl1234~");

    private String secret;

    JwtSecrets(String secret) {
        this.secret = secret;
    }

}
