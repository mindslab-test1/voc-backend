package ai.maum.voc.config.spring.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * ai.maum.voc.config.spring.security.jwt
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-17
 */
@Component
public class JwtSignHandler {

    @Value("${jwt.access-expired-time}")
    private Long expiredTime;

    public String createAccessToken(String userId) {
        Date date = new Date();
        return JWT.create()
                .withClaim("userId", userId)
                .withExpiresAt(new Date(date.getTime()+expiredTime))
                .withIssuedAt(date)
                .sign(Algorithm.HMAC256(JwtSecrets.MINDS_SECRET.getSecret()));
    }

}
