package ai.maum.voc.config.spring.security.jwt;

import ai.maum.voc.exception.UnauthorizedException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * ai.maum.voc.config.spring.security.jwt
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-17
 */
@Component
public class JwtVerifyHandler {

    public Mono<DecodedJWT> check(String accessToken) {
        try {
            return Mono.just(JWT.require(Algorithm.HMAC256(JwtSecrets.MINDS_SECRET.getSecret())).build().verify(accessToken));
        }catch (AlgorithmMismatchException e){
            return Mono.error(new UnauthorizedException(HttpStatus.UNAUTHORIZED, "유효하지 않은 토큰입니다."));
        }catch (TokenExpiredException e){
            return Mono.error(new UnauthorizedException(HttpStatus.UNAUTHORIZED, "만료된 토큰입니다."));
        }
    }

}
