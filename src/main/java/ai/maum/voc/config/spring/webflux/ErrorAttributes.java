package ai.maum.voc.config.spring.webflux;

import ai.maum.voc.exception.ApplicationException;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;

import java.util.Map;

/**
 * 에러속성
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-12
 */
@Component
public class ErrorAttributes extends DefaultErrorAttributes {

    @Override
    public Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions options) {
        Map<String, Object> map = super.getErrorAttributes(request, options);
        if (getError(request) instanceof ApplicationException) {
            ApplicationException ex = (ApplicationException) getError(request);
            map.put("exception", ex.getClass().getSimpleName());
            switch (ex.getClass().getSimpleName()){
                case "EntityNotFoundException" :
                    map.put("message", String.format("Entity 조회 중 오류(%s)가 발생했습니다.", ex.getReason()));
                    break;
                case "UnauthorizedException" :
                case "BadCredentialsException" :
                    map.put("message", ex.getReason());
                    break;
            }
            map.put("status", ex.getStatus().value());
            return map;
        }
        return map;
    }

}
