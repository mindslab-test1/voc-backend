package ai.maum.voc.config.spring.webflux;

import ai.maum.voc.application.login.service.LoginService;
import ai.maum.voc.config.spring.webflux.annotation.LoginUserInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.core.MethodParameter;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.BindingContext;
import org.springframework.web.reactive.result.method.HandlerMethodArgumentResolver;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Optional;

/**
 * ai.maum.voc.config.spring.webflux
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-18
 */
@Component
@RequiredArgsConstructor
public class UserInfoResolver implements HandlerMethodArgumentResolver {

    private final LoginService loginService;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return Optional.ofNullable(parameter.getParameterAnnotation(LoginUserInfo.class)).isPresent();
    }

    @Override
    public Mono<Object> resolveArgument(MethodParameter parameter, BindingContext bindingContext, ServerWebExchange exchange) {
        return ReactiveSecurityContextHolder.getContext()
                .switchIfEmpty(Mono.empty())
                .flatMap(securityContext -> loginService.getUserInfo(String.valueOf(securityContext.getAuthentication().getPrincipal())));
    }

}
