package ai.maum.voc.exception;

import org.springframework.http.HttpStatus;

/**
 * ai.maum.voc.exception
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-18
 */
public class BadCredentialsException extends ApplicationException {
    public BadCredentialsException(HttpStatus status) {
        super(status);
    }

    public BadCredentialsException(HttpStatus status, String reason) {
        super(status, reason);
    }

    public BadCredentialsException(HttpStatus status, String reason, Throwable cause) {
        super(status, reason, cause);
    }
}
