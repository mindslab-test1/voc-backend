package ai.maum.voc.exception;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * ai.maum.voc.exception
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-12
 */
public class EntityNotFoundException extends ApplicationException implements Serializable {

    private static final long serialVersionUID = 7314379581032689300L;

    public EntityNotFoundException(HttpStatus status) {
        super(status);
    }

    public EntityNotFoundException(HttpStatus status, String reason) {
        super(status, reason);
    }

    public EntityNotFoundException(HttpStatus status, String reason, Throwable throwable) {
        super(status, reason, throwable);
    }

}
