package ai.maum.voc.exception;

import org.springframework.http.HttpStatus;

/**
 * ai.maum.voc.exception
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-18
 */
public class JsonTokenParseException extends ApplicationException {
    public JsonTokenParseException(HttpStatus status) {
        super(status);
    }

    public JsonTokenParseException(HttpStatus status, String reason) {
        super(status, reason);
    }

    public JsonTokenParseException(HttpStatus status, String reason, Throwable cause) {
        super(status, reason, cause);
    }
}
