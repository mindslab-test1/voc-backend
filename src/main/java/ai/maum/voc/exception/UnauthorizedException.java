package ai.maum.voc.exception;

import org.springframework.http.HttpStatus;

/**
 * ai.maum.voc.exception
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-17
 */
public class UnauthorizedException extends ApplicationException {

    public UnauthorizedException(HttpStatus status) {
        super(status);
    }

    public UnauthorizedException(HttpStatus status, String reason) {
        super(status, reason);
    }

    public UnauthorizedException(HttpStatus status, String reason, Throwable cause) {
        super(status, reason, cause);
    }

}
